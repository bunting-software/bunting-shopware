<?php

use Shopware\Components\CSRFWhitelistAware;

class Shopware_Controllers_Backend_BuntingPersonalisation extends Enlight_Controller_Action implements CSRFWhitelistAware
{
    private $shop_repository = null;
    private $default_shop = null;
    private $analytics_repository = null;

    public function indexAction() {
        if ($this->buntingInstalled()) {
            return $this->redirect('/backend/BuntingPersonalisation/home');
        }

        $default_shop = $this->getDefaultShop();

        $shopName = Shopware()->Config()->get('shopName');
        $this->View()->assign([
            'shop_owner_email' => Shopware()->Config()->get('mail'),
            'shop_name' => $shopName,
            'potential_subdomain' => strtolower(preg_replace("/[^A-Za-z0-9]/", '', $shopName))
        ]);
    }

    public function homeAction() {
        if (!$this->buntingInstalled()) {
            return $this->redirect('/backend/BuntingPersonalisation/');
        }
        $bunting_repo = Shopware()->Models()->getRepository('Shopware\CustomModels\Bunting\Bunting');
        $bunting_data = $bunting_repo->findAll()[0];
        $timestamp = time();
        $bunting_subdomain = $bunting_data->getBuntingSubdomain();
        $bunting_email = $bunting_data->getBuntingEmail();
        $account_key = $bunting_subdomain.$bunting_email.$timestamp;
        $message = '';
        if (isset($_SESSION['message'])) {
            $message = $_SESSION['message'];
            unset($_SESSION['message']);
        }
        $this->View()->assign([
            'bunting_subdomain' => $bunting_subdomain,
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $account_key, 'Y7d-dJ}E3%$-da8'),
            'password_api' => $bunting_data->getPasswordApi(),
            'email_address' => $bunting_email,
            'message' => $message
        ]);
    }

    public function loginAction() {
        $bunting_data = $this->submitToBunting('verify',[
            'email_address' => $this->Request()->getParam('verify_email_address', null),
            'password' => $this->Request()->getParam('verify_password', null),
            'subdomain' => $this->Request()->getParam('verify_bunting_subdomain', null)
        ]);
        $this->buntingResponse($bunting_data);
    }

    public function registerAction() {
        $default_shop = $this->getDefaultShop();

        $full_locale = $default_shop->getLocale()->getLocale();
        list($language, $locale) = explode('_',$full_locale);

        $address = Shopware()->Config()->get('address');
        $address = $this->processAddress($address);

        $this->shop_repository = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop');

        $submit_data = [
            'billing' => 'automatic',
            'email_address' => $this->Request()->getParam('register_email_address', null),
            'password' => $this->Request()->getParam('register_password', null),
            'confirm_password' => $this->Request()->getParam('password_confirmation', null),
            'subdomain' => $this->Request()->getParam('register_bunting_subdomain', null),
            'name' => $this->Request()->getParam('company_name', null),
            'forename' => $this->Request()->getParam('forename', null),
            'surname' => $this->Request()->getParam('surname', null),
            'telephone_number' => $this->Request()->getParam('telephone_number', null),
            'promotional_code' => $this->Request()->getParam('promotional_code', null),
            'timezone' => date_default_timezone_get(),
            'country' => $locale,
            'agency' => 'no',
            'approximate_page_views_per_month' => $this->getAverageMonthlyPageviews()
        ];

        $submit_data = $submit_data+$address;

        $bunting_data = $this->submitToBunting('register',$submit_data);
        $this->buntingResponse($bunting_data);
    }

    private function processAddress($orig_address) {
        $comma_address = preg_replace('#\s+#',',',trim($orig_address));
        $address_array = explode(',', $comma_address);
        $address = [];
        if (!empty($address_array)) {
            $address['address_line_1'] = array_shift($address_array);
            $address['postcode'] = array_pop($address_array);
            $i = 2;
            foreach($address_array as $address_line) {
                $address['address_line_'.$i] = $address_line;
                if ($i < 5) {
                    $i++;
                }
                else {
                    break;
                }
            }
        }
        else {
            $address['address_line_1'] = $comma_address;
        }
        ksort($address);
        return $address;
    }

    private function buntingResponse($bunting_data) {
        if ($bunting_data['success']) {
            $response_array = ['message'=>'Success'];
            $_SESSION['message'] = 'You can now login to Bunting.';
        }
        else {
            $response_array = ['message'=>'Please review the errors and try again.', 'errors'=>$bunting_data['errors']];
        }
        $this->View()->assign(['json'=>json_encode($response_array)]);
    }

    public function unlinkAction() {
        $q = Shopware()->Models()->createQuery('delete from Shopware\CustomModels\Bunting\Bunting');
        $q->execute();
        $this->redirect('/backend/BuntingPersonalisation/unlinked');
    }

    private function submitToBunting($action, $params) {
        $default_shop = $this->getDefaultShop();
        $active_shops = $this->getShopRepository()->getActiveShops();

        $languages = [];
        $currencies = [];
        foreach($active_shops as $active_shop) {
            $full_locale = $active_shop->getLocale()->getLocale();
            list($language, $locale) = explode('_',$full_locale);
            $languages[$language] = strtoupper($language);
            $currency = $active_shop->getCurrency();
            $currencies[$currency->getCurrency()] = [
                'currency' => $currency->getCurrency(),
                'symbol' => html_entity_decode($currency->getSymbol())
            ];
            if ($active_shop->getDefault()) {
                foreach($active_shop->getCurrencies() as $currency) {
                    $currencies[$currency->getCurrency()] = [
                        'currency' => $currency->getCurrency(),
                        'symbol' => html_entity_decode($currency->getSymbol())
                    ];
                }
            }
        }

        $languages = array_values($languages);
        $currencies = array_values($currencies);

        $cart_url = Shopware()->Front()->Router()->assemble(['sViewport' => 'checkout', 'action' => 'cart','module'=>'frontend']);

        $timestamp = time();

        $feed_token = md5(Shopware()->Config()->get('esdKey').$timestamp);
        $default_params = [
            'timestamp' => $timestamp,
            'hash' => hash_hmac('sha256', $timestamp, 'Y7d-dJ}E3%$-da8'),
            'plugin' => 'shopware',
            'domain_name' => $default_shop->getHost(),
            'create_website_monitor' => 'yes',
            'website_name' => $default_shop->getName(),
            'languages' => $languages,
            'currencies' => $currencies,
            'website_platform' => 'Shopware',
            'ecommerce' => 'yes',
            'cart_url' => $cart_url,
            'product_feed-url_protocol' => 'https://',
            'product_feed-url' => $default_shop->getHost().'/bunting?feed_token='.$feed_token
        ];

        $params = $params+$default_params;
        $params = $this->http_build_query_for_curl($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://api.bunting.com/plugins/'.$action);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);

        if ($response === false) {
            $responseData = ['errors' => ['network' => 'Unable to connect to Bunting api: ' . curl_error($ch)]];
        } else {
            $responseData = json_decode($response, true);
        }

        if ($responseData['success']) {
            $responseData['email_address'] = $params['email_address'];
            $this->installBunting($responseData, $feed_token);
        }
        return $responseData;
    }

    private function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {
        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
        return $new;
    }

    private function installBunting($data, $feed_token) {
        $bunting = new Shopware\CustomModels\Bunting\Bunting();
        $bunting->setBuntingEmail($data['email_address']);
        $bunting->setBuntingAccountId($data['account_id']);
        $bunting->setBuntingWebsiteMonitorId($data['website_monitor_id']);
        $bunting->setBuntingUniqueCode($data['unique_code']);
        $bunting->setBuntingSubdomain($data['subdomain']);
        $bunting->setFeedToken($feed_token);
        $bunting->setPasswordApi($data['password_api']);
        Shopware()->Models()->persist($bunting);
        Shopware()->Models()->flush();
    }

    private function getShopRepository() {
        if (is_null($this->shop_repository)) {
            $this->shop_repository = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop');
        }
        return $this->shop_repository;
    }

    private function getDefaultShop() {
        if (is_null($this->default_shop)) {
            $this->default_shop = $this->getShopRepository()->getDefault();
        }
        return $this->default_shop;
    }

    private function getAnalyticsRepository() {
        if (!$this->analytics_repository) {
            $this->analytics_repository = new Shopware\Models\Analytics\Repository(
                $this->get('models')->getConnection(),
                $this->get('events')
            );
        }

        return $this->analytics_repository;
    }

    private function getAverageMonthlyPageViews() {
        $analytics_repository = $this->getAnalyticsRepository();

        $one_year_ago = new DateTime('-1 year');
        $months = [];
        $from = null;
        $to = null;
        for ($i = 1; $i <= 12; $i++) {
            if ($i == 1) {
                $from = new DateTime($one_year_ago->format('Y-m').'-01');
            }
            else {
                $from = new DateTime($to->modify('+1 day')->format('Y-m-d 00:00:00'));
            }
            $to = new DateTime($from->format('Y-m-t 23:59:59'));
            $months['month_'.$i] = [
                'from' => clone $from,
                'to' => clone $to
            ];
        }
        $total = 0;
        $months_with_views = 0;
        foreach($months as $month) {
            $visitor_impressions = $analytics_repository->getVisitorImpressions(0,10000, $month['from'], $month['to']);
            $data = $visitor_impressions->getData();
            if (!empty($data)) {
                foreach($data as $day) {
                    $total += $day['totalImpressions'];
                }
                $months_with_views++;
            }
        }

        return $total / $months_with_views;

    }

    private function buntingInstalled() {
        $query = Shopware()->Models()->createQueryBuilder()
            ->select('COUNT(b.id)')
            ->from('Shopware\CustomModels\Bunting\Bunting', 'b')
            ->getQuery();
        return $query->getSingleScalarResult();
    }

    public function unlinkedAction() {

    }

    public function getWhitelistedCSRFActions()
    {
        return [
            'index',
            'home',
            'login',
            'unlink',
            'unlinked',
            'register'
        ];
    }
}
