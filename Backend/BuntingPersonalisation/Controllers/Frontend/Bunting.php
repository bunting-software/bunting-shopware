<?php

class Shopware_Controllers_Frontend_Bunting extends Enlight_Controller_Action
{

    public function indexAction()
    {
        if ($feed_token = $this->Request()->getParam('feed_token', null)) {
            $bunting_repo = Shopware()->Models()->getRepository('Shopware\CustomModels\Bunting\Bunting');
            $bunting_data = $bunting_repo->findAll()[0];
            if ($bunting_data && $feed_token == $bunting_data->getFeedToken()) {
                $this->Response()->setHeader('Content-type', 'application/xml', true);
                
                $shop_categories = [];
                $shop_repo = Shopware()->Models()->getRepository('Shopware\Models\Shop\Shop');
                $active_shops = $shop_repo->getActiveShops();
                foreach($active_shops as $active_shop) {
                    $shop_category = $active_shop->getCategory();
                    $shop_categories[$shop_category->getId()]['shop'] = $active_shop;
                    $shop_categories[$shop_category->getId()]['category'] = $shop_category ;
                }

                // Pagination
                if ($this->Request()->getParam('size')) {
                    $limit = (int) $this->Request()->getParam('size');
                    if ($limit <= 0) {
                        $limit = 1;
                    }
                } else {
                    $limit = 200;
                }

                $start = $page = 0;
                if ($this->Request()->getParam('page')) {
                    $page = (int) $this->Request()->getParam('page');
                    if ($page > 0) {
                        $start = $page * $limit;
                    }
                }
                
                $products = [];

                $article_repo = Shopware()->Models()->getRepository('Shopware\Models\Article\Article');

                $qb = $article_repo->createQueryBuilder('products');
                $qb->where('products.active = ?1')
                    ->setParameter(1, '1');
                $q = $qb->getQuery();
                $q->setFirstResult($start)
                    ->setMaxResults($limit);
                $product_models = Shopware()->Models()->createPaginator($q);
                
                $product_count = 0;
                $names = [];
                $prices = [];
                foreach($product_models as $product_model) {
                    $categories = $product_model->getCategories();
                    $shops = $this->getShops($categories, $shop_categories);
                    $product = false;
                    foreach($shops as $shop) {
                        $full_locale = $shop->getLocale()->getLocale();
                        list($language, $locale) = explode('_',$full_locale);
                        $currency = strtolower($shop->getCurrency()->getCurrency());
                        $shop = $shop_repo->getActiveById($shop->getId());
                        Shopware()->Container()->set('shop', $shop);
                        Shopware()->Container()->get('shopware_storefront.context_service')->initializeShopContext();
                        Shopware()->Container()->get('shopware_storefront.context_service')->initializeProductContext();
                        $product = Shopware()->Modules()->Articles()->sGetArticleById($product_model->getId());
                        $names[$language] = $product['articleName'];
                        $prices[$currency] = $product['price_numeric'];
                    }
                    $main_category = $categories[0];
                    if (!$product || is_null($main_category) || is_null($product['image'])) {
                        continue;
                    }
                    $product['names'] = $names;
                    $product['prices'] = $prices;
                    $product['categoryName'] = $this->getCategoryBreadcrumb($main_category);
                    $products[] = $product;
                    $product_count++;
                }

                $last_page = 'yes';
                if ($product_count == $limit) {
                    $total = count($product_models); // This return the total amount without pagination

                    $last_page_number = ceil($total / $limit);

                    if (($page+1) != $last_page_number) {
                        $last_page = 'no';
                    }
                }
                
                $this->View()->assign([
                    'bunting_subdomain' => $bunting_data->getBuntingSubdomain(),
                    'bunting_website_monitor_id' => $bunting_data->getBuntingWebsiteMonitorId(),
                    'products' => $products,
                    'last_page' => $last_page
                ]);
            }
            else {
                $this->Response()->setHttpResponseCode(403);
                die('403 Forbidden');
            }
        }
        else {
            $this->Response()->setHttpResponseCode(400);
            die('400 Bad request');
        }
    }

    private function getCategoryBreadcrumb($category, $category_string = '') {
        if ($category) {
            if ($category_string != '') {
                $category_string = '>'.$category_string;
            }
            $category_string = $category->getName().$category_string;
            $parent_category = $category->getParent();
            if ($parent_category && !is_null($parent_category->getName())) {
                $category_string = $this->getCategoryBreadcrumb($parent_category, $category_string);
            }
        }
        else {
            $category_string = false;
        }
        return $category_string;
    }
    
    private function getShops($categories, $shop_categories, $shops = []) {
        $searched_cats = [];
        foreach($categories as $category) {
            $cat_id = $category->getId();
            if (!in_array($cat_id, $searched_cats)){
                if (isset($shop_categories[$cat_id])) {
                    $shop_id = $shop_categories[$cat_id]['shop']->getId();
                    if (!isset($shops[$shop_id])) {
                        $shops[$shop_id] = $shop_categories[$cat_id]['shop'];
                    }
                }
                $searched_cats[] = $cat_id;
                if (count($shops) == count($shop_categories)) {
                    return $shops;
                }
                $parent = $category->getParent();
                if ($parent) {
                    $shops = $this->getShops([$parent], $shop_categories, $shops);
                }
                if (count($shops) == count($shop_categories)) {
                    break;
                }
            }
        }
        return $shops;
    }

}