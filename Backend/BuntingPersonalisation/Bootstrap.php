<?php

class Shopware_Plugins_Backend_BuntingPersonalisation_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    static protected $userLoggedIn;

    private $bunting_repo = null;
    private $bunting_data = null;

    /**
     * Returns an array with the capabilities of the plugin.
     *
     * @return array
     */
    public function getCapabilities()
    {
        return array(
            'install' => true,
            'enable' => true,
            'update' => true
        );
    }

    public function getVersion()
    {
        return '1.0.2';
    }

    public function getLabel()
    {
        return 'Personalisation and Product Recommendation';
    }

    public function install()
    {
        try {
            // Frontend installation
            $this->subscribeEvent(
                'Enlight_Controller_Action_PostDispatch',
                'addTrackingCodes'
            );
            $this->registerController('frontend', 'bunting');

            //Backend installation
            $this->registerBackendController();
            $this->createMenuItem(array(
                'label' => 'Bunting Personalization',
                'onclick' => 'createSimpleModule("BuntingPersonalisation", { "title": "Bunting Website Personalization &amp; Product Recommendation" })',
                'class' => 'sprite-application-block',
                'action' => 'Index',
                'active' => 1,
                'parent' => $this->Menu()->findOneBy(['label' => 'Marketing'])
            ));

            $this->updateSchema();
            return array(
                'success' => true,
                'invalidateCache' => array('backend')
            );

        } catch (Exception $e) {
            return array('success' => false, 'message' => $e->getMessage());
        }
    }

    protected function updateSchema()
    {
        $this->registerCustomModels();

        $em = $this->Application()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\Bunting\Bunting')
        );

        try {
            $tool->dropSchema($classes);
        } catch (Exception $e) {
            //ignore
        }
        $tool->createSchema($classes);
    }

    public function uninstall()
    {
        $this->registerCustomModels();

        $em = $this->Application()->Models();
        $tool = new \Doctrine\ORM\Tools\SchemaTool($em);

        $classes = array(
            $em->getClassMetadata('Shopware\CustomModels\Bunting\Bunting')
        );
        $tool->dropSchema($classes);

        return true;
    }

    public function addTrackingCodes(\Enlight_Event_EventArgs $args) {
        if (!$this->buntingInstalled()) {
            return;
        }
        $request = $args->getSubject()->Request();
        $response = $args->getSubject()->Response();
        $view = $args->getSubject()->View();

        // Load this code only in frontend
        if(!$request->isDispatched() || $response->isException() || ($request->getModuleName()!='frontend' && $request->getModuleName()!="widgets")) {
            return;
        }
        Shopware()->Container()->get('template')->addTemplateDir($this->Path() . 'Views/');

        if (!isset($_SESSION['bunting'])) {
            $bunting_data = $this->getBuntingData();
            $_SESSION["bunting"] = [
                'subdomain' => $bunting_data->getBuntingSubdomain(),
                'website_monitor_id' => $bunting_data->getBuntingWebsiteMonitorId(),
                'unique_code' => $bunting_data->getBuntingUniqueCode()
            ];
        }

        if(self::$userLoggedIn === null){
            self::$userLoggedIn = Shopware()->Modules()->Admin()->sCheckUser();
        }

        $controller = $request->getControllerName();
        $action = $request->getActionName();

        if ($view->hasTemplate()){
            $view->assign("sUserLoggedIn",self::$userLoggedIn);
            if (self::$userLoggedIn) {
                $sUserData = Shopware()->Modules()->Admin()->sGetUserData();
                $view->sUserData = $sUserData;
            }
            if ($controller == 'index' && $action == 'index') {
                $view->homePage = true;
            }
            elseif ($controller == 'listing' && $action == 'index') {
                $view->categoryPage = true;
                $category_repo = Shopware()->Models()->getRepository('Shopware\Models\Category\Category');
                $category = $category_repo->find($view->sCategoryInfo['id']);
                $view->categoryString = $this->getCategoryBreadcrumb($category);
            }
            elseif ($controller == 'detail' && $action == 'index') {
                $view->productPage = true;
            }
            elseif ($controller == 'checkout' && $action == 'cart') {
                $view->cartPage = true;
            }
            elseif ($controller == 'checkout' && $action == 'finish') {
                $view->checkoutSuccessPage = true;
            }
            if (strpos($request->getRequestUri(), 'checkout') !== false) {
                $view->checkoutPage = true;
            }
        }
    }

    private function getCategoryBreadcrumb($category, $category_string = '') {
        if ($category) {
            if ($category_string != '') {
                $category_string = '>'.$category_string;
            }
            $category_string = $category->getName().$category_string;
            $parent_category = $category->getParent();
            if ($parent_category && !is_null($parent_category->getName())) {
                $category_string = $this->getCategoryBreadcrumb($parent_category, $category_string);
            }
        }
        else {
            $category_string = false;
        }
        return $category_string;
    }

    private function getBuntingRepo() {
        if (is_null($this->bunting_repo)) {
            $this->registerCustomModels();
            $this->bunting_repo = Shopware()->Models()->getRepository('Shopware\CustomModels\Bunting\Bunting');
        }
        return $this->bunting_repo;
    }

    private function getBuntingData() {
        if (is_null($this->bunting_data)) {
            $bunting_repo = $this->getBuntingRepo();
            $this->bunting_data = $bunting_repo->findAll()[0];
        }
        return $this->bunting_data;
    }

    private function buntingInstalled() {
        return !is_null($this->getBuntingData());
    }

    public function registerBackendController()
    {
        $this->subscribeEvent(
            'Enlight_Controller_Dispatcher_ControllerPath_Backend_BuntingPersonalisation',
            'onGetBackendController'
        );
    }

    /**
     * Returns the path to the controller.
     *
     * Event listener function of the Enlight_Controller_Dispatcher_ControllerPath_Backend_SwagFavorites
     * event.
     * Fired if an request will be root to the own Favorites backend controller.
     *
     * @return string
     */
    public function onGetBackendController()
    {
        $this->get('template')->addTemplateDir($this->Path() . 'Views/');
        $this->registerCustomModels();

        return $this->Path() . 'Controllers/BuntingPersonalisation.php';
    }
}
