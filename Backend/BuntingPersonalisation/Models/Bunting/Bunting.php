<?php

namespace Shopware\CustomModels\Bunting;

use Shopware\Components\Model\ModelEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="s_bunting")
 */
class Bunting extends ModelEntity
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $bunting_email
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $bunting_email;

    /**
     * @var string $bunting_account_id
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $bunting_account_id;

    /**
     * @var string $bunting_website_monitor_id
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $bunting_website_monitor_id;

    /**
     * @var string $bunting_unique_code
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $bunting_unique_code;

    /**
     * @var string $bunting_subdomain
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $bunting_subdomain;

    /**
     * @var string $feed_token
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $feed_token;

    /**
     * @var string $password_api
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $password_api;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $bunting_email
     */
    public function setBuntingEmail($bunting_email)
    {
        $this->bunting_email = $bunting_email;
    }

    /**
     * @return string
     */
    public function getBuntingEmail()
    {
        return $this->bunting_email;
    }

    /**
     * @param string $bunting_account_id
     */
    public function setBuntingAccountId($bunting_account_id)
    {
        $this->bunting_account_id = $bunting_account_id;
    }

    /**
     * @return string
     */
    public function getBuntingAccountId()
    {
        return $this->bunting_account_id;
    }

    /**
     * @param string $bunting_website_monitor_id
     */
    public function setBuntingWebsiteMonitorId($bunting_website_monitor_id)
    {
        $this->bunting_website_monitor_id = $bunting_website_monitor_id;
    }

    /**
     * @return string
     */
    public function getBuntingWebsiteMonitorId()
    {
        return $this->bunting_website_monitor_id;
    }

    /**
     * @param string $bunting_unique_code
     */
    public function setBuntingUniqueCode($bunting_unique_code)
    {
        $this->bunting_unique_code = $bunting_unique_code;
    }

    /**
     * @return string
     */
    public function getBuntingUniqueCode()
    {
        return $this->bunting_unique_code;
    }

    /**
     * @param string $bunting_subdomain
     */
    public function setBuntingSubdomain($bunting_subdomain)
    {
        $this->bunting_subdomain = $bunting_subdomain;
    }

    /**
     * @return string
     */
    public function getBuntingSubdomain()
    {
        return $this->bunting_subdomain;
    }

    /**
     * @param string $feed_token
     */
    public function setFeedToken($feed_token)
    {
        $this->feed_token = $feed_token;
    }

    /**
     * @return string
     */
    public function getFeedToken()
    {
        return $this->feed_token;
    }

    /**
     * @param string $password_api
     */
    public function setPasswordApi($password_api)
    {
        $this->password_api = $password_api;
    }

    /**
     * @return string
     */
    public function getPasswordApi()
    {
        return $this->password_api;
    }

}