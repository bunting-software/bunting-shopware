# Installing Bunting on Shopware

To download the plugin click on Downloads in the left hand menu then click the `Download Repository` link.

Next to install the `bunting-shopware` plugin, go the backend dashboard of your site and within the menu on the top hover
over `Configuration` then select plugin manager or use the keyboard shortcut `Ctrl + Alt + P`. Then on the left hand menu go to
`Installed`, this will navigate you to a plugin listing page. On the top left of this page should be an `Upload plugin` button.

Here please upload the `buntingsoftware-buntingshopware.zip` if everything works correctly it should appear in the uninstalled list.

>Please note that this file must be a `.zip` file. If you downloaded the `buntingsoftware-buntingshopware` folder just zip up the
folder and then retry the upload.

Now select the `Install/Uninstall` button to the right, You may get prompted to clear your cache click yes and wait for the cache to clear.
Once this is completed it will move the plugin into the `inactive` list.

Select open and within the popup click `Activate`. Similar to before this will move the plugin
into the `Installed` list.

If activated successfully go to the top menu and hover over `Marketing` and click on `Bunting Personalization` and wait for the
plugin popup window to appear.

Sign in to your Bunting account to complete the installation process: Enter your existing bunting account details or
if you are new to Bunting and want to get started, click the `CREATE ACCOUNT` button.