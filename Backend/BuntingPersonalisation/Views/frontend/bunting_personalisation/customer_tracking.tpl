<!-- Additional visitor information - place ABOVE Generic Head Code -->
{literal}
<script type="text/javascript">
    if (typeof window.$_Bunting=='undefined') window.$_Bunting={d:{}}; // Do not edit

    // 1) Unique account number / code when a visitor is logged in to their account on your site
    $_Bunting.d.uac = "{/literal}{$sUserData.additional.user.customerId}{literal}";

    // 2) Known visitor information
    $_Bunting.d.fn = "{/literal}{$sUserData.billingaddress.firstname}{literal}"; // The visitor's forename, if known
    $_Bunting.d.sn = "{/literal}{$sUserData.billingaddress.lastname}{literal}"; // The visitor's surname, if known
    $_Bunting.d.ea = "{/literal}{$sUserData.additional.user.email}{literal}"; // The visitor's email address, if known
    $_Bunting.d.g = "";  // The visitor's gender. Enter 'male' or 'female' if known, or leave empty

</script>
{/literal}