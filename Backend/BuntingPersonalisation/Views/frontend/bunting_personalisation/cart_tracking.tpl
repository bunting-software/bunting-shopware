<!-- Bunting shopping cart script - place ABOVE Generic Head Code -->
{literal}
<script type="text/javascript">
    if (typeof $_Bunting=="undefined") var $_Bunting={d:{}}; // Do not edit
    $_Bunting.d.cp = new Array(); // Do not edit

    var shipping = {/literal}{$sShippingcosts}{literal};
    shipping = shipping.toFixed(2);

    $_Bunting.d.cdc = shipping.toString();  // Recommended. Delivery cost of the cart (number)

    // Repeat the following code for each product in cart - start here

        {/literal}{foreach $sBasket.content as $sBasketItem}
            {if $sBasketItem.articleID != 0}{literal}
            var price = "{/literal}{$sBasketItem.price}{literal}";
            price = checkDecimal(price);

            $_Bunting.d.cp.push([
                {/literal}
                "{$sBasketItem.articleID}",
                price.toString(),
                "{$sBasketItem.quantity}"
                {literal}
            ]);
            {/literal}{/if}
        {/foreach}{literal}
    // End cart product repetition here

    function checkDecimal(price) {
        var decimal_mark = price.charAt(price.length-3);
        if (decimal_mark == ',') {
            price = price.replace(",", "*");
            price = price.replace(".", "");
            price = price.replace("*", ".");
        }
        return price;
    }
</script>
{/literal}