{* BuntingPersonalisation/Views/frontend/index/header.tpl *}
{extends file="parent:frontend/index/header.tpl"}

{block name='frontend_index_header_meta_http_tags' append}
    {if $sUserLoggedIn}
        {include file="frontend/bunting_personalisation/customer_tracking.tpl"}
    {/if}
    {if $homePage}
        {include file="frontend/bunting_personalisation/home_tracking.tpl"}
    {/if}
    {if $categoryPage}
        {include file="frontend/bunting_personalisation/category_tracking.tpl"}
    {/if}
    {if $productPage}
        {include file="frontend/bunting_personalisation/product_tracking.tpl"}
    {/if}
    {if $cartPage}
        {include file="frontend/bunting_personalisation/cart_tracking.tpl"}
    {/if}
    {if $checkoutPage}
        {include file="frontend/bunting_personalisation/checkout_tracking.tpl"}
    {/if}
    {if $checkoutSuccessPage}
        {include file="frontend/bunting_personalisation/checkout_success_tracking.tpl"}
    {/if}
    {include file="frontend/bunting_personalisation/bunting_tracking.tpl"}
{/block}

