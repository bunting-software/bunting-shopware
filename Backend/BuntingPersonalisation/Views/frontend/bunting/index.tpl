<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE feed SYSTEM "https://{$bunting_subdomain}.bunting.com/feed-{$bunting_website_monitor_id}.dtd">
<feed last_page="{$last_page}">

    {foreach from=$products item=product}
    <product>
        <upc>{$product.articleID}</upc>
        <ns>
            {foreach from=$product.names key=language item=name}
            <{$language}><![CDATA[{$name}]]></{$language}>
            {/foreach}
        </ns>
        <ps>
            {foreach from=$product.prices key=currency item=price}
            <{$currency}>{$price}</{$currency}>
            {/foreach}
        </ps>
        <u><![CDATA[{$product.linkDetailsRewrited}]]></u>
        {if $product.image}<iu><![CDATA[{$product.image.source}]]></iu>{/if}

        {if $product.images}<i2u><![CDATA[{$product.images[0].source }]]></i2u>{/if}

        {if $product.categoryName}<c><![CDATA[{$product.categoryName}]]></c>{/if}

        {if $product.supplierName}<b><![CDATA[{$product.supplierName}]]></b>{/if}

        {if $product.instock}<s>{$product.instock}</s>{else}<s>n</s>{/if}

        {if $product.keywords}<kw><![CDATA[{$product.keywords}]]></kw>{/if}

        {if $product.ean}<gtin><![CDATA[{$product.ean}]]></gtin>{/if}

    </product>
    {/foreach}

</feed>