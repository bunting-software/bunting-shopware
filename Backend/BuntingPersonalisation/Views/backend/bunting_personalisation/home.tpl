{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
    <div class="box-container logged-in">
        {if $message != ''}
            <h2 class="title">Bunting added <strong>Successfully</strong></h2>
            <p>{$message}</p>
        {else}
            <h2 class="title">Sign in to <strong>Bunting</strong></h2>
        {/if}
        <p>
            Account address:<br><strong>{$bunting_subdomain}.bunting.com</strong>
        </p>
        <form action="https://{$bunting_subdomain}.bunting.com/login" class="dotted go-to-bunting" target="_blank" method="post">
            <input type="hidden" name="a" value="login">
            <input type="hidden" name="timestamp" value="{$timestamp}">
            <input type="hidden" name="hash" value="{$hash}">
            <input type="hidden" name="password_api" value="{$password_api}">
            <input type="hidden" name="email_address" value="{$email_address}">
            <input type="hidden" name="plugin" value="shopware">
            {if $message != ''}
                <input type="hidden" name="redirect" value="/account?a=show_registration_complete">
            {/if}
            <button type="submit" class="btn btn-info">
                Login to your<br>Bunting Account
            </button>
        </form>
        <a href="/backend/BuntingPersonalisation/unlink" onclick="return confirm('Are you sure you want to unlink your Bunting account?');" class="unlink-bunting btn btn-default">
            Uninstall
        </a>
    </div>
{/block}

{block name="content/javascript" append}
<script>
    setInterval(function() {
        window.location.reload();
    }, 58000);
</script>
{/block}
