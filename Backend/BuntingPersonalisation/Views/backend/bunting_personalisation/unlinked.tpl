{extends file="parent:backend/_base/layout.tpl"}

{block name="content/main"}
    <div class="box-container logged-in">
        <h2 class="title">Bunting scripts <strong>removed successfully</strong></h2>
        <p>The Bunting tracking script has been successfully removed from your store. Bunting is no longer tracking your visitors.</p>
        <br>
        <h3>Uninstalling the app</h3>
        <p>Go to Configuration -> Plugin Manager. Search for the Bunting Personalization plugin, click on it and then click the Uninstall button.</p>
        <br>
        <p><strong>We're sorry to see you go, but thank you for using Bunting!</strong></p>
        <br>
        <a href="/backend/BuntingPersonalisation" class="btn btn-primary btn-lg" >Back to the install page</a>
    </div>
{/block}