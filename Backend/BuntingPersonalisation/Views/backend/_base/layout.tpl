<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{link file="backend/_resources/css/bootstrap.min.css"}">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet" />
    <style>
        BODY {
            font-family:'Open Sans', Arial, Helvetica, sans-serif;
            font-size:14px;
            font-weight:400;
            line-height:20px;
            padding:0 0 20px 0;
            margin:0;
            display:block;
            position:relative;
            background-color:transparent;
        }

        BODY * {
            font-family:'Open Sans', Arial, Helvetica, sans-serif;
            font-weight:400;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: Raleway, Arial, sans-serif;
            margin-bottom: 15px;
        }
        h3 {
            border-bottom: 1px solid #CCCCCC;
            padding-bottom: 10px;
            font-size: 22px;
            margin-top: 0;
        }
        .box-container {
            background: #fff;
            max-width: 350px;
            margin: 0 auto;
            text-align: center;
            border-radius: 5px 5px 5px 5px;
            box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.3);
            position: relative;
        }
        .box-container.wide {
            max-width: 700px;
        }
        .box-container .title {
            margin: 0;
            padding: 20px 0;
            font-size: 25px;
            background: #EFEFEF;
            margin-bottom: 20px;
            border-radius: 5px 5px 0 0;
        }
        .box-container .title strong {
            display: block;
        }
        .box-container #plans {
            margin-bottom: 15px;
        }
        .login {
            margin-top: 20px;
            padding: 0 30px;
        }
        .dotted:after {
            display: inline-block;
            padding-top: 10px;
            content: ".......................................................";
            color: #ddd;
            font-size: 20px;
            max-width: 90%;
            overflow: hidden;
        }
        @media screen and (min-width: 600px) {
            html, body {
                min-height: 100%;
            }
            .box-container {
                background: #fff;
                padding: 0 40px 40px 40px;
                margin: 40px auto;
            }
            .box-container .title {
                margin: 0 -40px 20px;
                padding: 20px 40px;
            }
        }
        .box-container h2 strong {
            font-weight: bold;
            display: block;
        }
        .login h2,
        .register h2 {
            margin-top: 0;
            margin-bottom: 15px;
            font-size: 16px;
        }
        #loginForm,
        .logged-in {
            position: relative;
        }
        #registerForm form {
            background: #eaeaea;
            padding: 10px;
            margin-top: 15px;
        }
        #registerForm form fieldset {
            background: #fff;
            border: 1px dashed #CCCCCC;
            margin-bottom: 10px;
            padding: 26px 26px 11px 26px;
        }
        .back {
            position: absolute;
            top: 37px;
            left: 30px;
        }
        .submit {
            border-top: 1px solid #EAEAEA;
            padding-bottom: 10px;
            padding-top: 10px;
        }
        .submit-text {
            color: #666666;
            font-size: 12px;
            margin-top: 6px;
        }
        .submit-button {
            text-align: right;
        }
        .content {
            padding: 15px;
        }
        .tick-list {
            padding-left: 0;
        }
        .tick-list li {
            list-style: none;
            padding-left: 30px;
            background: url('https://getbunting.com/media/icons/tick.gif') 5px 2px no-repeat;
            margin-bottom: 5px;
            font-size: 18px;
        }
        .form-group {
            text-align: left;
        }
        label {
            font-weight: normal;
        }
        .form-group label:first-of-type:after {
            content: ": ";
        }
        .form-group.required label:first-of-type:before {
            content: "* ";
            color: #CC0000;
        }
        label.error {
            color: #CC0000;
            display: block;
            margin: -10px 0 10px;
        }
        .form-group label.error {
            margin: 5px 0px 0px;
        }
        .form-group .hint {
            color: #999999;
            font-size: 11px;
            margin-top: 3px;
            display: block;
        }
        .btn {
            background-color:#008DFD;
            background:-webkit-linear-gradient(top, #008DFD 0px, #0370EA 100%);
            border:1px solid #076BD2;
            border-radius:3px;
            color:#FFFFFF !important;
            display:inline-block;
            font-size:14px;
            font-weight:700;
            line-height:1.3;
            padding:12px 25px !important;
            text-align:center;
            text-decoration:none !important;
            user-select: none;
            margin-right:5px;
            text-transform:uppercase;
            white-space: normal;
        }

        .btn.btn-primary {
            background-color:#398B00;
            background:-webkit-linear-gradient(top, #3C9200 0px, #398B00 100%);
            border:1px solid #286E11;
        }
        .btn.btn-info {
            background-color:#008DFD;
            background:-webkit-linear-gradient(top, #008DFD 0px, #0370EA 100%);
            border:1px solid #076BD2;
        }
        .btn.btn-danger {
            background-color:#D24836;
            background:-webkit-linear-gradient(top, #DC4B39 0px, #D24836 100%);
            border:1px solid #D04432;
        }
        .btn.btn-default {
            background-color:#999999;
            background:-webkit-linear-gradient(top, #AAAAAA 0px, #999999 100%);
            border:1px solid #999999;
        }
        .btn-xl {
            font-size: 22px;
        }

        .btn.large {
            font-size:20px;
            line-height:24px;
            padding:14px 30px !important;
            font-weight:400;
        }
        /* Logged in */
        div.logged-in {
            margin-top: 40px;
            text-align: center;
            min-width: 250px;
        }
        .logo {
            font-size: 0;
            height: 60px;
            background: url('{link file="backend/_resources/images/logo.png"}') 40% 50% no-repeat;
        }
        .go-to-bunting {
            margin-bottom: 15px;
        }
        .field-error,
        .form-control.error {
            background: #f2dede;
            border-color: #ebccd1;
            color: #a94442;
        }
        #loading {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba(255, 255, 255, 0.8);
            z-index: 1000;
            text-align: center;
            display: none;
        }
        #loading:before {
            content: "";
            display: inline-block;
            height: 100%;
            width: 0;
            vertical-align: middle;
        }

        .spinner {
            display: inline-block;
            width: 70px;
            text-align: center;
        }

        .spinner > div {
            width: 18px;
            height: 18px;
            background-color: #333;

            border-radius: 100%;
            display: inline-block;
            -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
            animation: sk-bouncedelay 1.4s infinite ease-in-out both;
        }

        .spinner .bounce1 {
            -webkit-animation-delay: -0.32s;
            animation-delay: -0.32s;
            background-color: #F0776C;
        }

        .spinner .bounce2 {
            -webkit-animation-delay: -0.16s;
            animation-delay: -0.16s;
            background-color: #62C2E4;
        }

        .spinner .bounce3 {
            background-color: #FECF71;
        }

        @-webkit-keyframes sk-bouncedelay {
            0%, 80%, 100% { -webkit-transform: scale(0) }
            40% { -webkit-transform: scale(1.0) }
        }

        @keyframes sk-bouncedelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            } 40% {
                  -webkit-transform: scale(1.0);
                  transform: scale(1.0);
              }
        }
    </style>
</head>
<body role="document">
<div id="loading">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<div class="body-inner">
    {block name="content/main"}{/block}
</div> <!-- /container -->

<script type="text/javascript" src="{link file="backend/base/frame/postmessage-api.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/jquery-2.1.4.min.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/bootstrap.min.js"}"></script>
<script type="text/javascript" src="{link file="backend/_resources/js/jquery.validate.min.js"}"></script>

{block name="content/layout/javascript"}
    <script type="text/javascript">
        $(document).ready(function() {
            $('#billing-freemium, #billing-automatic').addClass('btn btn-primary');
        });
        $('#choosePlanButton').click(function() {
            $('#loginForm').fadeOut(function() {
                $('#registerForm').fadeIn();
            });

        });
        $('#registerForm .back').click(function() {
            $('#registerForm').fadeOut(function() {
                $('#loginForm').fadeIn();
            });
        });
        jQuery.validator.addMethod("subdomain", function(value, element) {
            return this.optional( element ) || /^[a-z0-9\-_]+$/.test( value );
        }, "Please specify a valid subdomain, using lowercase letters, numbers, hyphens and underscores.");
        $("#actualLoginForm").validate({
            rules: {
                verify_bunting_subdomain: {
                    required: true,
                    maxlength: 50
                },
                verify_email_address: {
                    required: true,
                    email: true
                },
                verify_password: {
                    required: true,
                    maxlength: 100
                }
            },
            submitHandler: function(form) {
                submitForm(form, 'login', 'verify');
            }
        });
        $("#actualRegisterForm").validate({
            rules: {
                register_email_address: {
                    required: true,
                    email: true
                },
                register_password: {
                    required: true,
                    minlength: 8,
                    maxlength: 100
                },
                password_confirmation: {
                    required: true,
                    equalTo: "#registerForm #register_password"
                },
                forename: {
                    required: true,
                    maxlength: 100
                },
                surname: {
                    required: true,
                    maxlength: 100
                },
                company_name: {
                    required: true,
                    maxlength: 100
                },
                register_subdomain: {
                    required: true,
                    subdomain: true,
                    maxlength: 100
                },
                promotional_code: {
                    maxlength: 20
                }
            },
            submitHandler: function(form) {
                submitForm(form, 'register', 'register');
            }
        });
        function submitForm(form, type, prefix) {
            $('#loading').show();
            var $message = $('p.message');
            $message.hide();
            $('label.error').hide();
            $('input.error').removeClass('error');

            var $this = $(form),
                values = $this.serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});

            $.ajax({
                type: "POST",
                url: '/backend/BuntingPersonalisation/'+type,
                appendCSRFToken: true,
                data: values,
                dataType: 'json',
                success: function(data) {
                    if (typeof data.errors === 'undefined') {
                        window.location.href = '/backend/BuntingPersonalisation/home';
                    }
                    else {
                        window.scrollTo(0, 0);
                        $message.show().html(data.message);
                        console.log(data.errors);
                        for (var property in data.errors) {
                            console.log(property);
                            if (data.errors.hasOwnProperty(property)) {
                                var value = data.errors[property];
                                if (property == 'subdomain') {
                                    property = prefix+'_bunting_subdomain';
                                }
                                else if (property == 'email_address' || property == 'password') {
                                    property = prefix+'_'+property;
                                }
                                else if (property == 'name') {
                                    property = 'company_name';
                                }
                                else if (property == 'confirm_password') {
                                    property = 'password_confirmation';
                                }
                                else if (property == 'validation') {
                                    property = 'verify_password';
                                    $('#verify_email_address').addClass('error');
                                }
                                $('#'+property).addClass('error').parent().after('<label class="error">'+value+'</label>');
                            }
                        }
                    }
                    $('#loading').hide();
                },
                always: function() {
                    $('#loading').hide();
                }
            });
        }
    </script>
{/block}
{block name="content/javascript"}{/block}
</body>
</html>
